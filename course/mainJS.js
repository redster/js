            //CRASH COURSE JAVASCRIPT


// String, Number, Boolean, Null, undefined

const name = 'John';
const age = 30;

// Concatination

console.log('my name is ' + name + ' and i am ' + age);

// Template String

const hello = `my name is ${name} and i am ${age}`;
console.log(hello);

const s = 'hello world, computers, mouse, keyboard';

console.log(s.length);
console.log(s.toUpperCase());
console.log(s.substring(0, 5).toUpperCase());
console.log(s.split(','));


// ARRAYS 

const fruits = ['apples', 'orange', 'pears', 10, true];

fruits[3] = 'grapes';

fruits.push('mangos');

fruits.unshift('strawberries');

fruits.pop();

console.log(fruits.indexOf('orange'))
console.log(fruits);

const person = {
    fn: 'john',
    ln: 'doe',
    age: 30,
    hobbies: ['music', 'movies', 'sports'],
    address: {
        city: 'spb',
        street: 'mainStreet'
    }
}

// DESTUCTURING

console.log(person.hobbies[2]);

const { fn, ln, address: {city, street}} = person;

console.log(`${city} ${street}`);

const todos = [
    {
        id: 1,
        text: 'Take out trash',
        isCompleted: true
    },
    {
        id: 2,
        text: 'Meeting with boss',
        isCompleted: true
    },
    {
        id: 3,
        text: 'Dentist appt',
        isCompleted: false
    },
];

console.log(todos[1].text);

//*****************/

const todoJSON = JSON.stringify(todos);
console.log(todoJSON);

//****************** */


//for

for(let i = 0; i < todos.length; i++) {
    console.log(todos[i].text)
}

//while

let i = 0;
while(i < 10) {
    console.log(`while loop number ${i}`)
    i++;
}

// forEach, map, filter

todos.forEach(function(todo){
    console.log(todo.text);
});

const todoText = todos.map(function(todo){
    console.log(todo.text);
});

//FILTER

const todoCompleted = todos.filter(function(todo) {
    return todo.isCompleted === false;
}).map(function(todo){
    return todo.text;
})    
    console.log(todoCompleted);

let array1 = [1,2,3,4,5, -1];

let c = array1.filter(function(Value, Index){
    return Index > 0;
});
console.log(c);

// CONDITIONALS

const x = 10;

if(x === 10) {
    console.log('x is 10');
}   else {
    console.log('x is not 10');
}

const y = 11;

if(y === 10) {
    console.log('y is 10');
}  
 else if (y > 10) {
    console.log('y more then 10');
}
else {
    console.log('y is not 10');
}

const x1 = 4
const x2 = 11;

if(x1 > 5 || x2 > 10) {
    console.log('x is more than 5 or y is 10');
}

// TERNAR OP

const a = 10;

const color = a > 10 ? 'red' : 'blue';

console.log(color);

switch(color) {
    case 'red':
        console.log('color is red');
        break;
    case 'blue':
        console.log('color is blue');
        break;
    default:
        console.log('color is not red or blue');
        break;
}

// FUNCTION 

function addNums(num1, num2) {
    console.log(num1 + num2);    
}

addNums(5,4);

//OR

function addNums(num11 = 1, num22 = 2) {
    console.log(num11 + num22);    
}

addNums();

// ARROW FUNC

const AddsNumber = nums1 => nums1 + 5;
    console.log(AddsNumber(10))


// OBJECTS 

// constructor 

function Person(firstName, lastName, dob) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.dob = new Date(dob);
    this.getBirthYear = function() {
        return this.dob.getFullYear();
    }
    this.getFullName = function() {
        return `${this.firstName} ${this.lastName}`;
    }
}


// instantiate obj

const person1 = new Person('John', 'Doe', '4-5-1980');
const person2 = new Person('Mary', 'Smith', '2-4-1980');

console.log(person2.dob);
console.log(person1.getBirthYear());
console.log(person1.getFullName());

// Prototype

Person.prototype.getBirthYear = function() {
    return this.dob.getFullYear();
}

Person.prototype.getFullName = function() {
    return `${this.firstName} ${this.lastName}`;
}

console.log(person2.getFullName())
console.log(person1)

//CLASS 

class Personal {
    constructor(firstName, lastName, dob) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = new Date(dob);
    }

    getBirthYear() {
        return this.dob.getFullYear();
    }

    getFullName() {
        return `${this.firstName} ${this.lastName}`;
    }
}

